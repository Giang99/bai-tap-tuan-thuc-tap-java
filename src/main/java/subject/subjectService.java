package subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.plaf.basic.BasicComboPopup.ListDataHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class subjectService {
	@Autowired
	  subjectRepository sjRepository;

	  public Optional<subject> getSubject(String maMon) {
	    return sjRepository.findById(maMon);
	  }

	  public void updateSubject(String maMon,String tenMon,String BMPhutrach,String gioiThieu,subject sj) {
	    sj.setmaMon(maMon);
	    sj.settenMon(tenMon);
	    sj.setBMPhutrach(BMPhutrach);
	    sj.setgioiThieu(gioiThieu);
	    sjRepository.save(sj);
	  }

	
	  public void createSubject(String maMon,String tenMon,String BMPhutrach,String gioiThieu,subject sj) {
		  	sj.setmaMon(maMon);
		    sj.settenMon(tenMon);
		    sj.setBMPhutrach(BMPhutrach);
		    sj.setgioiThieu(gioiThieu);
		    sjRepository.save(sj);
	  }

	 
	  public List<subject> getAllsubjects() {
	    List<subject> subjects = new ArrayList<subject>();
	    sjRepository.findAll().forEach(subjects::add);
	    return subjects;
	  }

	 
	  public List<subject> findsubjectBytenMon(String tenMon) {
	    List<subject> subjects = new ArrayList<>();
	    sjRepository.findBytenMon(tenMon).forEach(subjects::add);
	    return subjects;
	  }

	 
	  public List<subject> findsubjectBymaMon(String maMon) {
	    return sjRepository.findBymaMon(maMon);
	  }

}
