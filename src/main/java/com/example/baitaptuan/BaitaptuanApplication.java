package com.example.baitaptuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaitaptuanApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaitaptuanApplication.class, args);
	}

}
